﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.ViewModels
{
    class LoginViewModel
    {
        public Employee SelectedEmployee { get; set; }
        public List<Employee> Employees
        {
            get { return Controller.Employees; }
        }

        public bool Auth(string password)
        {
            if (Controller.Auth(SelectedEmployee != null ? SelectedEmployee.Id : -1, password))
            {
                MainWindow window = new MainWindow();
                window.Show();
                return true;
            }
            return false;
        }
    }
}
