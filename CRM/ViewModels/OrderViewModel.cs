﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CRM
{
    class OrderViewModel : INotifyPropertyChanged
    {
        private Order order = new Order();
        private string nameSearch = "";
        private int? minimalWorth;
        private List<Order> orders = Controller.Orders;
        private int? maximalWorth;
        private DateTime? minTime;
        private DateTime? maxTime;
        private bool onlyMine;
        private UserControl content = new Views.MainMenuView();
        private Stack<UserControl> controls = new Stack<UserControl>();
        private bool fromStack = false;
        private Client currentClient;
        private Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"name", ""},
            {"phone", ""},
            {"email", ""},
            {"complex", ""}
        };
        public string ComplexFilter
        {
            get { return filters["complex"]; }
            set { SetClientFilter("complex", value); }
        }
        public string EmailFilter
        {
            get { return filters["email"]; }
            set { SetClientFilter("email", value); }
        }
        public string PhoneFilter
        {
            get { return filters["phone"]; }
            set { SetClientFilter("phone", value); }
        }
        public string NameFilter
        {
            get { return filters["name"]; }
            set { SetClientFilter("name", value); }
        }
        public Visibility BackElementVisibility
        {
            get 
            { 
                if (controls.Count == 0) return Visibility.Hidden;
                return Visibility.Visible;
            }
        }
        public List<Order> MyOrders
        {
            get
            {
                return Controller.Orders.Where(q => q.Seller.Id == Controller.CurrentUser.Id).Distinct().ToList();
            }
        }
        public void SetClientFilter(string filter, string value)
        {
            if (filters.ContainsKey(filter))
            {
                filters[filter] = value;
                OnPropertyChanged("Clients");
                OnPropertyChanged(char.ToUpper(filter[0]) + filter.Substring(1) + "Filter");
            }
        }
        public void ResetClientFilters()
        {
            var keys =  new List<string>(filters.Keys);
            foreach (var item in keys)
            {
                SetClientFilter(item, "");
            }
        }
        public List<Client> Clients
        {
            get
            {
                if (filters.Count(q => !string.IsNullOrEmpty(q.Value)) == 0)
                    return Controller.Clients;
                return Controller.Clients.Where(q =>               
                    (((q.Name ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"]))
                    || ((q.Phone ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"]))
                    || ((q.Email ?? "").IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0  && !string.IsNullOrEmpty(filters["complex"]))
                    || (q.Id.ToString().IndexOf(filters["complex"], StringComparison.InvariantCultureIgnoreCase) >= 0 && !string.IsNullOrEmpty(filters["complex"])))
                    && ((q.Name ?? "").IndexOf(filters["name"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["name"]))
                    && ((q.Phone ?? "").IndexOf(filters["phone"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["phone"]))
                    && ((q.Email ?? "").IndexOf(filters["email"], StringComparison.InvariantCultureIgnoreCase) >= 0 || string.IsNullOrEmpty(filters["email"]))).ToList();
            }
        }
        public List<Client> MyClients
        {
            get
            {
                return MyOrders.Select(q => q.Client).ToList();
            }
        }

        public Client CurrentClient
        {
            get { return currentClient; }
            set
            {
                currentClient = value;
                OnPropertyChanged("CurrentClient");
            }
        }
        public UserControl ContentWindow
        {
            get { return content; }
            set
            {
                if (!fromStack)
                {
                    controls.Push(content);
                    OnPropertyChanged("BackElementVisibility");
                }
                content = value;
                fromStack = false;
                OnPropertyChanged("ContentWindow");
            }
        }
        public UserControl LastControl
        {
            get 
            {
                if (controls.Count != 0)
                {
                    fromStack = true;
                    var c = controls.Pop();
                    OnPropertyChanged("BackElementVisibility");
                    return c;
                }
                else return ContentWindow;
            }
        }

        public DateTime? MinDate
        {
            get { return minTime; }
            set
            {
                minTime = value;
                OnPropertyChanged("MinDate");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public DateTime? MaxDate
        {
            get { return maxTime; }
            set
            {
                maxTime = value;
                OnPropertyChanged("MaxDate");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public bool OnlyMine
        {
            get { return onlyMine; }
            set
            {
                onlyMine = value;
                OnPropertyChanged("OnlyMine");
                OnPropertyChanged("CurrentOrders");
            }
        }

        public OrderViewModel()
        {
            Controller.StaticPropertyChanged += OrdersData_StaticPropertyChanged;
        }

        void OrdersData_StaticPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("CurrentOrders");
        }

        public List<Order> CurrentOrders 
        {
            get { return OnlyMine ? 
                Controller.Orders.Where(q =>
                    q.Seller == Controller.CurrentUser && q.Id.ToString().Contains(NameSearch)).ToList() :
                Controller.Orders.Where(q => q.Id.ToString().Contains(NameSearch)).ToList(); }
        }
        public string NameSearch
        {
            get { return nameSearch; }
            set
            {
                nameSearch = value;
                OnPropertyChanged("NameSearch");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public int? MinimalWorth
        {
            get { return minimalWorth; }
            set
            {
                minimalWorth = value;
                OnPropertyChanged("MinimalWorth");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public int? MaximalWorth
        {
            get { return maximalWorth; }
            set
            {
                maximalWorth = value;
                OnPropertyChanged("MaximalWorth");
                OnPropertyChanged("CurrentOrders");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void UpdateOrders()
        {

        }
    }
}
