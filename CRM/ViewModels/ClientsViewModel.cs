﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    class ClientsViewModel
    {
        public List<Client> Clients
        {
            get { return Controller.Clients.ToList(); }
        }
    }
}
