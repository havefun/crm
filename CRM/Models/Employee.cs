﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        private string Password { get; set; }

        public Employee()
        {
            Password = "111";
        }

        public bool Login(string password)
        {
            return password == Password;
        }
    }
}
