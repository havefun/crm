﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public Client()
        {
            Id = -1;
        }

        private List<Order> orders = new List<Order>();
        public List<Order> Orders
        {
            get 
            {
                if (orders == null || orders.Count == 0)
                    orders = Controller.Orders.Where(q => q.Client.Id == Id).ToList();
                return orders;
            }
            set
            {
                orders = null;
            }
        }
        public void ClearOrdersData()
        {
            orders = new List<Order>();
        }
    }
}
