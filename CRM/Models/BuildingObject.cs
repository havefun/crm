﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM
{
    public enum BuildingObjectStatus
    {
        Free,
        Reserved,
        Sold
    }

    public enum BuildingType
    {
        Dacha,
        House,
        Banya,
        Other
    }
    public class BuildingObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BuildingObjectStatus Status { get; set; }
        public BuildingType Type { get; set; }
        public int Floors { get; set; }
        public int Rooms { get; set; }
        public double Square { get; set; }
        public string Address { get; set; }
        public DateTime BuiltYear { get; set; }
        public string Materials { get; set; }
        public bool Electricity { get; set; }
        public bool Sewers { get; set; }
        public bool WaterCommunications { get; set; }
        public bool Heating { get; set; }
        public bool Gas { get; set; }
        public double YardSquare { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }

        public string FullDescription
        {
            get
            {
                return string.Format("");
            }
        }
    }

//-Номер 
//-Наименование 
//-Статус 
//-Тип строения 
//-Этажность 
//-Количество комнат 
//-Площадь 
//-Адрес 
//-Год постройки 
//-Материалы 
//-Внутренняя отделка 
//-Электричество 
//-Канализация 
//-Вода 
//-Отопление 
//-Газ 
//-Площщадь участка 
//-Фото 
//-Описание 

}