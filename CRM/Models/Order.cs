﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM
{
    public enum OrderStatus
    {
        New,
        Visiting,
        Confirmed,
        Rejected,
        Finished
    }
    public class Order : INotifyPropertyChanged
    {
        public long Id { get; set; }
        private BuildingObject _object;
        public BuildingObject BuildingObject 
        {
            get { return _object; }
            set 
            { 
                _object = value;
                OnPropertyChanged("BuildingObject");
            }
        }
        public DateTime? CreationDate { get; set; }
        public DateTime? FinishDate { get; set; }
        private Client _client { get; set; }
        public Client Client
        {
            get { return _client; }
            set
            {
                _client = value;
                OnPropertyChanged("Client");
            }
        }
        public Employee Seller { get; set; }
        public OrderStatus Status { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
