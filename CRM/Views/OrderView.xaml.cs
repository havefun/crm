﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CRM
{
    /// <summary>
    /// Логика взаимодействия для OrderView.xaml
    /// </summary>
    public partial class OrderView : Window
    {
        public OrderView()
        {
            InitializeComponent();
        }
        public bool IsNew;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var order = (DataContext as Order);
            if (!IsNew)
                Controller.Remove(order);
            else
            {
                Controller.Add((DataContext as Order));
            }
            this.Close();
        }

        private void Button_Loaded(object sender, RoutedEventArgs e)
        {
            if (!IsNew)
                ActionButton.Content = "Удалить";
        }

        private void SellerTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }


        private void ObjectTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var objectView = new ObjectsListView();
            objectView.Order = (DataContext as Order);
            objectView.LastSelection = objectView.Order.BuildingObject;
            objectView.ShowDialog();
        }

        private void Client_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var clientView = new ClientsListView();
            clientView.Order = (DataContext as Order);
            clientView.LastSelection = clientView.Order.Client;
            clientView.ShowDialog();
        }
    }
}
