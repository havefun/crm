﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRM.Views
{
    /// <summary>
    /// Логика взаимодействия для MainMenuView.xaml
    /// </summary>
    public partial class MainMenuView : UserControl
    {
        public Client client;

        public MainMenuView()
        {
            InitializeComponent();
        }

        private void NewClientButton_Click(object sender, RoutedEventArgs e)
        {
            var client = new Client { Name = AddClientFullNameTextBox.Text, Phone = AddClientPhoneTextBox.Text, Email = AddClientEmailTextBox.Text };
            var saveResult = Controller.AddOrGet(client);
            (DataContext as OrderViewModel).CurrentClient = client;
            if (saveResult != null)
            {
                var clientExists = new Views.ClientAlreadyExistsView();
                clientExists.DataContext = saveResult;
                var createNew = clientExists.ShowDialog() ?? false;
                if (createNew)
                {
                    Controller.Add(client);
                    (DataContext as OrderViewModel).CurrentClient = client;
                }
                else
                    (DataContext as OrderViewModel).CurrentClient = saveResult;
            }

            (this.DataContext as OrderViewModel).ContentWindow = new Views.MainMenuOrderSelectionView();
        }

        private void SearchClientButton_Click(object sender, RoutedEventArgs e)
        {
            var context = (this.DataContext as OrderViewModel);
            context.ContentWindow = new Views.MainMenuClientSearchResultView();
        }

        private void SearchOrderButton_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as OrderViewModel).ContentWindow = new Views.MainMenuOrderSearchResultsView();
        }
    }
}
