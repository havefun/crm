﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CRM
{
    /// <summary>
    /// Логика взаимодействия для ObjectsListView.xaml
    /// </summary>
    public partial class ObjectsListView : Window
    {
        public BuildingObject LastSelection = null;
        public Order Order = null;
        public ObjectsListView()
        {
            InitializeComponent();
        }

        private void Data_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Order.BuildingObject = Data.SelectedItem as BuildingObject;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Order.BuildingObject = LastSelection;
            this.Close();
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Data.SelectedItem == null) return;
            var selectedObj = Data.SelectedItem as BuildingObject;

            ObjectView obj = new ObjectView();
            //obj.IsNew = false;
            obj.DataContext = Controller.Get(selectedObj);
            obj.ShowDialog();
        }
    }
}
